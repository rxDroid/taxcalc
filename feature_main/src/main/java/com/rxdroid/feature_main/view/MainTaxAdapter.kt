package com.rxdroid.feature_main.view

import android.view.ViewGroup
import androidx.collection.SparseArrayCompat
import androidx.recyclerview.widget.RecyclerView
import com.rxdroid.feature_base.adapter.AdapterConstants
import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_base.adapter.ViewTypeDelegateAdapter

class MainTaxAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: ArrayList<ItemViewType> = ArrayList()
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()

    init {
        delegateAdapters.put(AdapterConstants.MAIN_VIEW_ITEM, MainInfoDelegateAdapter())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder = delegateAdapters.get(viewType)
        if (viewHolder != null) {
            return viewHolder.onCreateViewHolder(parent)
        }
        throw RuntimeException("ViewHolder not found. Missing delegate adapter!")
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int) = this.items[position].getItemViewType()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = delegateAdapters.get(getItemViewType(position))
        viewHolder?.onBindViewHolder(holder, this.items[position])
    }

    fun replaceItems(taxList: List<ItemViewType>) {
        items.clear()
        items.addAll(taxList)
        notifyItemRangeChanged(0, items.size)
    }

}
