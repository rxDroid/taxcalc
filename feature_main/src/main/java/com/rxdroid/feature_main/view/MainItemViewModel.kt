package com.rxdroid.feature_main.view

import com.rxdroid.feature_base.adapter.AdapterConstants
import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_base.formatCurrency
import com.rxdroid.feature_base.formatPercentage
import com.rxdroid.lib_repository.Tax

data class MainItemViewModel(
    val revenueIncome: String = "",
    val revenueTax: String = "",
    val grossIncome: String = "",
    val netIncome: String = "",
    val netIncomePercentage: String = "",
    val incomeTax: String = "",
    val incomePercentage: String = "",
    val solidarityTax: String = "",
    val solidarityPercentage: String = "",
    val allTaxes: String = "",
    val allTaxesPercentage: String = "",
    val year: Int = 0,
    val bookingYear: String = "",
    val bookingEntries: String = "",
    val clickListener: (ClickAction) -> Unit
) : ItemViewType {

    companion object {

        fun create(
            revenueIncomeByYear: Double,
            revenueTaxByYear: Double,
            grossIncomeByYear: Double,
            size: Int,
            bookingYear: Int,
            clickListener: (ClickAction) -> Unit
        ): MainItemViewModel {
            val incomeTaxValue = Tax.Income.calculateTax(grossIncomeByYear)
            val solidarityTax = Tax.Solidarity.calculateTax(incomeTaxValue)
            val netIncome = revenueIncomeByYear - incomeTaxValue - solidarityTax - revenueTaxByYear
            val incomePercent = (incomeTaxValue / grossIncomeByYear) * 100
            val solidarityPercent = (solidarityTax / grossIncomeByYear) * 100
            val netPercentage = 100 - (incomePercent + solidarityPercent)
            return MainItemViewModel(
                revenueIncome = formatCurrency(revenueIncomeByYear),
                revenueTax = formatCurrency(revenueTaxByYear),
                grossIncome = formatCurrency(grossIncomeByYear),
                year = bookingYear,
                bookingYear = bookingYear.toString(),
                incomeTax = formatCurrency(incomeTaxValue),
                incomePercentage = formatPercentage(incomePercent),
                solidarityTax = formatCurrency(solidarityTax),
                solidarityPercentage = formatPercentage(solidarityPercent),
                netIncome = formatCurrency(netIncome),
                netIncomePercentage = formatPercentage(netPercentage),
                allTaxes = formatCurrency(solidarityTax + incomeTaxValue),
                allTaxesPercentage = formatPercentage(incomePercent + solidarityPercent),
                bookingEntries = "$size Buchungen",
                clickListener = clickListener
            )
        }
    }

    override fun getItemViewType(): Int = AdapterConstants.MAIN_VIEW_ITEM

    fun onClick() {
        clickListener.invoke(ClickAction.OnItem(year))
    }

}
