package com.rxdroid.feature_main.view

import androidx.lifecycle.viewModelScope
import androidx.navigation.ActionOnlyNavDirections
import com.rxdroid.feature_base.view.StorageViewError
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.viewmodel.BaseViewModel
import com.rxdroid.feature_main.R
import com.rxdroid.lib_repository.TaxRepository
import com.rxdroid.lib_repository.base.DataOperation
import com.rxdroid.lib_repository.base.RepoError
import com.rxdroid.lib_repository.base.Result
import com.rxdroid.lib_repository.data.Income
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.collections.LinkedHashMap

@Suppress("EXPERIMENTAL_API_USAGE")
class MainViewModel @Inject constructor(
    private val taxRepository: TaxRepository
) : BaseViewModel<MainViewData>(MainViewData()) {

    fun loadData() {
        viewModelScope.launch {
            taxRepository.loadAll()
                .catch { emit(Result.Error(RepoError.create(DataOperation.READ, it))) }
                .onStart {
                    update {
                        copy(viewState = ViewState.LOADING)
                    }
                }
                .transform { emit(mapIncomeResults(it, ::performClick)) }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            handleSuccess(result.data)
                        }
                        is Result.Error -> {
                            handleError(result.repoError)
                        }
                    }
                }
        }
    }

    private fun handleSuccess(results: List<MainItemViewModel>) {
        if (results.isEmpty()) {
            update {
                copy(
                    viewState = ViewState.NO_RESULTS,
                    viewItemModels = emptyList(),
                    viewError = null
                )
            }
        } else {
            update {
                copy(
                    viewState = ViewState.DATA_LOADED,
                    viewItemModels = results,
                    viewError = null
                )
            }
        }
    }

    private fun handleError(repoError: RepoError) {
        update {
            copy(
                viewState = ViewState.ERROR,
                viewError = StorageViewError.create(repoError)
            )
        }
    }

    private fun mapIncomeResults(
        resultIncomes: Result<List<Income>>,
        clickListener: (ClickAction) -> Unit
    ): Result<List<MainItemViewModel>> {
        return when (resultIncomes) {
            is Result.Success -> {
                Result.Success(createItemViewModels(resultIncomes.data, clickListener))
            }
            is Result.Error -> {
                Result.Error(resultIncomes.repoError)
            }
        }
    }

    fun onAddIncomeClick() {
        navigate(ActionOnlyNavDirections(R.id.nav_action_mainFragment_to_add_income_fragment))
    }

    fun onAddExpenseClick() {
        navigate(ActionOnlyNavDirections(R.id.nav_action_mainFragment_to_add_expense_fragment))
    }

    private fun performClick(action: ClickAction) = when (action) {
        is ClickAction.OnItem -> {
            onItemClicked(action.year)
        }
        else -> { //nothing for now
        }
    }

    private fun onItemClicked(bookingYear: Int) {
        val navAction = MainFragmentDirections.navActionMainFragmentToDetailsFragment(bookingYear)
        navigate(navAction)
    }

    private fun createItemViewModels(
        incomes: List<Income>,
        clickListener: (ClickAction) -> Unit
    ): List<MainItemViewModel> {
        val filteredMap = LinkedHashMap<Int, MainItemViewModel>()
        incomes.forEach {
            val bookingYear = it.bookingDate.get(Calendar.YEAR)
            if (filteredMap.containsKey(bookingYear)) {
                val entries = entriesByYear(bookingYear, incomes)
                val updatedViewModel = updateViewModel(bookingYear, clickListener, entries)
                filteredMap[bookingYear] = updatedViewModel
            } else {
                filteredMap[bookingYear] = MainItemViewModel.create(
                    it.revenueIncome,
                    it.revenueTax,
                    it.grossIncome,
                    1,
                    bookingYear,
                    clickListener
                )
            }
        }
        return filteredMap.values
            .toList()
            .sortedByDescending { it.year }
    }

    private fun entriesByYear(
        bookingYear: Int,
        incomes: List<Income>
    ): List<Income> = incomes.filter { it.bookingDate.get(Calendar.YEAR) == bookingYear }

    private fun updateViewModel(
        bookingYear: Int,
        clickListener: (ClickAction) -> Unit,
        entriesByYear: List<Income>
    ): MainItemViewModel {
        var revenueIncomeByYear = 0.0
        var revenueTaxByYear = 0.0
        var grossIncomeByYear = 0.0
        entriesByYear.forEach {
            revenueIncomeByYear += it.revenueIncome
            revenueTaxByYear += it.revenueTax
            grossIncomeByYear += it.grossIncome
        }
        return MainItemViewModel.create(revenueIncomeByYear, revenueTaxByYear, grossIncomeByYear, entriesByYear.size, bookingYear, clickListener)
    }

}
