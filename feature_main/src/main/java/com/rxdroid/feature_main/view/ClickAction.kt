package com.rxdroid.feature_main.view

sealed class ClickAction {

    data class OnItem(val year: Int) : ClickAction()

    //TODO
    object OnRetry : ClickAction()

}
