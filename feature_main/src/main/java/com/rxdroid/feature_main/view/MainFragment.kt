package com.rxdroid.feature_main.view

import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_base.view.InjectableNavFragment
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.view.ViewState.*
import com.rxdroid.feature_base.view.hideKeyboard
import com.rxdroid.feature_base.visibleOrGone
import com.rxdroid.feature_main.R
import com.rxdroid.feature_main.databinding.FragmentMainBinding
import com.rxdroid.feature_base.view.ViewError

class MainFragment : InjectableNavFragment<FragmentMainBinding, MainViewModel>() {

    private val taxAdapter = MainTaxAdapter()

    private lateinit var binding: FragmentMainBinding

    override fun getViewModelClazz(): Class<MainViewModel> = MainViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_main

    override fun initBinding(binding: FragmentMainBinding) {
        binding.viewModel = getViewModel()
        binding.lifecycleOwner = viewLifecycleOwner
        binding.toolbar.title = "TaxCalc"
        this.binding = binding
        initRecyclerView()
        getViewModel().apply {
            viewData().observe(viewLifecycleOwner, ::updateUi)
            loadData()
        }
    }

    private fun initRecyclerView() {
        val recyclerView = binding.recyclerView
        recyclerView.adapter = taxAdapter
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
    }

    private fun updateUi(data: MainViewData) {
        toggleLoadingView(data.viewState)
        toggleResultView(data.viewState)
        toggleNoResultsView(data.viewState)
        toggleErrorView(data.viewState)
        when (data.viewState) {
            DATA_LOADED -> {
                showResults(data.viewItemModels)
            }
            ERROR -> {
                showError(data.viewError)
            }
            else -> {
                //nothing
            }
        }
    }

    private fun showError(viewError: ViewError?) {
        viewError?.let {
            binding.includedError.title.text = getText(it.getTitleResId())
            binding.includedError.message.text = getText(it.getMessageResId())
            binding.includedError.icon.setImageResource(it.getDrawableResId())
        }
    }

    private fun showResults(carViewModels: List<ItemViewType>) {
        taxAdapter.replaceItems(carViewModels)
    }

    private fun toggleLoadingView(viewState: ViewState) {
        binding.includedLoading.view.visibleOrGone(viewState == LOADING)
    }

    private fun toggleErrorView(viewState: ViewState) {
        binding.includedError.view.visibleOrGone(viewState == ERROR)
    }

    private fun toggleNoResultsView(viewState: ViewState) {
        binding.includedNoResults.view.visibleOrGone(viewState == NO_RESULTS)
    }

    private fun toggleResultView(viewState: ViewState) {
        binding.recyclerView.visibleOrGone(viewState == DATA_LOADED)
    }

}
