package com.rxdroid.feature_main.view

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.rxdroid.feature_base.adapter.DelegateAdapter
import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_main.BR
import com.rxdroid.feature_main.R

class MainInfoDelegateAdapter : DelegateAdapter(){

    override fun getLayoutId(): Int = R.layout.item_main_info

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ItemViewType) {
        val binding: ViewDataBinding? = DataBindingUtil.getBinding(holder.itemView)
        binding?.let {
            val resultVar: Boolean = it.setVariable(BR.viewModel, item)
            if (!resultVar) {
                throw RuntimeException("Missing binding variable!")
            }
            it.executePendingBindings()
        }
    }

}
