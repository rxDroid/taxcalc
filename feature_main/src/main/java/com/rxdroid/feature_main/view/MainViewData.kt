package com.rxdroid.feature_main.view

import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.view.ViewError

data class MainViewData(
    val viewState: ViewState = ViewState.INIT,
    val viewItemModels: List<ItemViewType> = emptyList(),
    val viewError: ViewError? = null
)
