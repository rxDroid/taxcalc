package com.rxdroid.feature_main.di

import com.rxdroid.feature_main.view.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentModule {

    @ContributesAndroidInjector(modules = [MainViewModule::class])
    abstract fun contributeHomeFragment(): MainFragment
}
