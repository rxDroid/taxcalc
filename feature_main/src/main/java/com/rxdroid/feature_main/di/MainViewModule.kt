package com.rxdroid.feature_main.di

import androidx.lifecycle.ViewModel
import com.rxdroid.feature_base.di.ViewModelKey
import com.rxdroid.feature_main.view.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun mainViewModel(viewModel: MainViewModel): ViewModel
}