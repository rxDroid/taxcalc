package com.rxdroid.feature_details

import android.util.Log
import androidx.lifecycle.viewModelScope
import androidx.navigation.ActionOnlyNavDirections
import com.rxdroid.feature_base.view.StorageViewError
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.viewmodel.BaseViewModel
import com.rxdroid.lib_repository.TaxRepository
import com.rxdroid.lib_repository.base.DataOperation
import com.rxdroid.lib_repository.base.RepoError
import com.rxdroid.lib_repository.base.Result
import com.rxdroid.lib_repository.data.Income
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

@Suppress("EXPERIMENTAL_API_USAGE")
class DetailsViewModel @Inject constructor(
    private val taxRepository: TaxRepository
) : BaseViewModel<DetailsViewData>(DetailsViewData()) {

    fun onBackPressed() {
        navigate(ActionOnlyNavDirections(R.id.nav_action_back_to_main))
    }

    fun loadData(bookingYear: Int) {
        viewModelScope.launch {
            taxRepository.loadAllByYear(bookingYear)
                .catch { emit(Result.Error(RepoError.create(DataOperation.READ, it))) }
                .onStart {
                    update {
                        copy(viewState = ViewState.LOADING)
                    }
                }
                .transform { emit(mapDetailResults(it)) }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            handleSuccess(result.data)
                        }
                        is Result.Error -> {
                            handleError(result.repoError)
                        }
                    }
                }
        }
    }

    private fun mapDetailResults(it: Result<List<Income>>): Result<List<DetailItemViewModel>> {
        return when (it) {
            is Result.Success -> {
                Result.Success(createItemViewModels(it.data))
            }
            is Result.Error -> {
                Result.Error(it.repoError)
            }
        }
    }

    private fun createItemViewModels(incomes: List<Income>): List<DetailItemViewModel> {
        val viewModels = ArrayList<DetailItemViewModel>()
        incomes.forEach {
            viewModels.add(
                DetailItemViewModel.create(it, ::performClick)
            )
        }
        return viewModels.sortedByDescending { it.bookingMonth }
    }

    private fun handleSuccess(results: List<DetailItemViewModel>) {
        if (results.isEmpty()) {
            update {
                copy(
                    viewState = ViewState.NO_RESULTS,
                    viewItemModels = emptyList(),
                    viewError = null
                )
            }
        } else {
            update {
                copy(
                    viewState = ViewState.DATA_LOADED,
                    viewItemModels = results,
                    viewError = null
                )
            }
        }
    }

    private fun handleError(error: RepoError) {
        update {
            copy(
                viewState = ViewState.ERROR,
                viewError = StorageViewError.create(error)
            )
        }
    }

    private fun performClick(clickAction: ClickAction) {
        when (clickAction) {
            is ClickAction.OnDetailItem -> {
                Log.d("FUCK", "performClick - clickAction.id: " + clickAction.id)
            }
        }

    }

}
