package com.rxdroid.feature_details.di

import com.rxdroid.feature_details.DetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DetailsFragmentModule {

    @ContributesAndroidInjector(modules = [DetailsViewModule::class])
    abstract fun contributeFragment(): DetailsFragment
}