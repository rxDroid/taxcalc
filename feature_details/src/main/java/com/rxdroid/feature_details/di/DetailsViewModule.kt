package com.rxdroid.feature_details.di

import androidx.lifecycle.ViewModel
import com.rxdroid.feature_base.di.ViewModelKey
import com.rxdroid.feature_details.DetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailsViewModule {

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun viewModel(viewModel: DetailsViewModel): ViewModel
}
