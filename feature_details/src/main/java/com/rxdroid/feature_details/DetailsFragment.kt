package com.rxdroid.feature_details

import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_base.view.InjectableNavFragment
import com.rxdroid.feature_base.view.ViewError
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.visibleOrGone
import com.rxdroid.feature_details.databinding.FragmentDetailsBinding


class DetailsFragment : InjectableNavFragment<FragmentDetailsBinding, DetailsViewModel>() {

    private val args: DetailsFragmentArgs by navArgs()
    private val detailAdapter = DetailAdapter()

    private lateinit var binding: FragmentDetailsBinding

    override fun getViewModelClazz(): Class<DetailsViewModel> = DetailsViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_details

    override fun initBinding(binding: FragmentDetailsBinding) {
        val bookingYear = args.bookingYear
        binding.viewModel = getViewModel()
        binding.lifecycleOwner = viewLifecycleOwner
        binding.toolbar.apply {
            setNavigationOnClickListener { getViewModel().onBackPressed() }
            title = bookingYear.toString()
        }
        this.binding = binding
        initRecyclerView()
        getViewModel().apply {
            viewData().observe(viewLifecycleOwner, ::updateUi)
            loadData(bookingYear)
        }
    }

    private fun initRecyclerView() {
        val recyclerView = binding.recyclerView
        recyclerView.adapter = detailAdapter
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    private fun updateUi(viewData: DetailsViewData) {
        toggleLoadingView(viewData.viewState)
        toggleResultView(viewData.viewState)
        toggleErrorView(viewData.viewState)
        when (viewData.viewState) {
            ViewState.DATA_LOADED -> {
                showResults(viewData.viewItemModels)
            }
            ViewState.ERROR -> {
                showError(viewData.viewError)
            }
            else -> {
                //nothing
            }
        }
    }

    private fun showResults(carViewModels: List<ItemViewType>) {
        detailAdapter.replaceItems(carViewModels)
    }

    private fun toggleResultView(viewState: ViewState) {
        binding.recyclerView.visibleOrGone(viewState == ViewState.DATA_LOADED)
    }

    private fun toggleLoadingView(viewState: ViewState) {
        binding.includedLoading.view.visibleOrGone(viewState == ViewState.LOADING)
    }

    private fun toggleErrorView(viewState: ViewState) {
        binding.includedError.view.visibleOrGone(viewState == ViewState.ERROR)
    }

    private fun showError(viewError: ViewError?) {
        viewError?.let {
            binding.includedError.title.text = getText(it.getTitleResId())
            binding.includedError.message.text = getText(it.getMessageResId())
            binding.includedError.icon.setImageResource(it.getDrawableResId())
        }
    }

}
