package com.rxdroid.feature_details

import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.view.ViewError

data class DetailsViewData(
    val viewState: ViewState = ViewState.INIT,
    val title: String = "2020",
    val viewItemModels: List<ItemViewType> = emptyList(),
    val viewError: ViewError? = null
)
