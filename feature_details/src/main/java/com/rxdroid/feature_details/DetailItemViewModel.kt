package com.rxdroid.feature_details

import com.rxdroid.feature_base.adapter.AdapterConstants
import com.rxdroid.feature_base.adapter.ItemViewType
import com.rxdroid.feature_base.formatCurrency
import com.rxdroid.feature_base.formatDate
import com.rxdroid.lib_repository.data.Income
import java.util.*

data class DetailItemViewModel(
    val id: Long,
    val revenueIncome: String = "",
    val revenueTax: String = "",
    val grossIncome: String = "",
    val bookingDate: String = "",
    val client: String = "",
    val bookingMonth: Int = 0,
    val clickListener: (ClickAction) -> Unit
) : ItemViewType {

    companion object {

        fun create(
            income: Income,
            clickListener: (ClickAction) -> Unit
        ): DetailItemViewModel = DetailItemViewModel(
            id = income.id ?: 0L,
            revenueIncome = formatCurrency(income.revenueIncome),
            revenueTax = formatCurrency(income.revenueTax),
            grossIncome = formatCurrency(income.grossIncome),
            bookingDate = formatDate(income.bookingDate.time),
            bookingMonth = income.bookingDate.get(Calendar.MONTH),
            client = income.client ?: "",
            clickListener = clickListener
        )
    }

    fun onClick() {
        clickListener.invoke(ClickAction.OnDetailItem(id))
    }

    override fun getItemViewType(): Int = AdapterConstants.DETAIL_VIEW_ITEM

}
