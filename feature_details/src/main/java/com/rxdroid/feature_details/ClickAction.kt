package com.rxdroid.feature_details

sealed class ClickAction {

    data class OnDetailItem(val id: Long) : ClickAction()

}
