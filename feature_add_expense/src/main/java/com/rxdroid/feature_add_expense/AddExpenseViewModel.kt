package com.rxdroid.feature_add_expense

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.ActionOnlyNavDirections
import com.rxdroid.feature_base.default
import com.rxdroid.feature_base.formatCurrency
import com.rxdroid.feature_base.formatDate
import com.rxdroid.feature_base.viewmodel.BaseViewModel
import com.rxdroid.feature_base.withAutoObserver
import com.rxdroid.lib_repository.AddExpenseRepository
import com.rxdroid.lib_repository.base.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@Suppress("EXPERIMENTAL_API_USAGE")
class AddExpenseViewModel @Inject constructor(
    private val addExpenseRepository: AddExpenseRepository
) : BaseViewModel<ExpenseViewData>(ExpenseViewData()) {

   /* val expenseData = MutableLiveData<String>()
    val categoryData = MutableLiveData<String>()

    private var expense: Double = 0.0
    private var category: String = ""
    private var bookingCalendar: Calendar = Calendar.getInstance()

    val viewData = MutableLiveData<ExpenseViewData>().default(
        ExpenseViewData(
            expense = formatCurrency(expense),
            category = category,
            bookingDate = formatDate(Date()),
            submitEnabled = false
        )
    )

    fun initialize(viewLifecycleOwner: LifecycleOwner) {
        expenseData.withAutoObserver(viewLifecycleOwner, ::onExpenseUpdate)
        categoryData.withAutoObserver(viewLifecycleOwner, ::onCategoryUpdate)
        viewData.value = viewData.value?.copy(bookingDate = formatDate(Date()))
    }

    fun onSubmitClicked() {
        viewData.value = viewData.value?.copy(submitEnabled = false)
        viewModelScope.launch {
            addExpenseRepository.storeExpense(expense, bookingCalendar)
                .catch { throwable ->
                    emit(Result.DataError(StorageViewError.InsertError(throwable)))
                }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            handleSuccess(result)
                        }
                        is Result.DataError -> {
                            handleError(result.storageError)
                        }
                    }
                }
        }
    }

    private fun handleError(storageError: StorageViewError) {
        //TODO show loading screen
        //TODO error handling
    }

    private fun handleSuccess(result: Result.Success<Boolean>) {
        if (result.data) {
            navigate(ActionOnlyNavDirections(R.id.nav_action_back_to_main))
        }
    }

    fun onDateClicked() {
        navigate(ActionOnlyNavDirections(R.id.nav_action_add_expense_fragment_to_datePickerFragment))
    }

    private fun onCategoryUpdate(category: String?) {

    }

    private fun onExpenseUpdate(value: String?) {
        if (!value.isNullOrEmpty()) {
            expense = value.toDouble()
            viewData.value = viewData.value?.copy(
                expense = formatCurrency(value.toDouble()),
                submitEnabled = true
            )
        } else {
            viewData.value = viewData.value?.copy(submitEnabled = false)
        }
    }

    fun updateDate(calendar: Calendar) {
        bookingCalendar = calendar
        viewData.value = viewData.value?.copy(bookingDate = formatDate(calendar.time))
    }
*/
}
