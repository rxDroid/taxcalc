package com.rxdroid.feature_add_expense

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.rxdroid.feature_base.view.InjectableNavFragment
import com.rxdroid.feature_base.viewmodel.SharedDataViewModel
import com.rxdroid.feature_add_expense.databinding.FragmentAddExpenseBinding

class AddExpenseFragment : InjectableNavFragment<FragmentAddExpenseBinding, AddExpenseViewModel>() {

    private lateinit var binding: FragmentAddExpenseBinding

    override fun getViewModelClazz(): Class<AddExpenseViewModel> = AddExpenseViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_add_expense

    override fun initBinding(binding: FragmentAddExpenseBinding) {
/*        requireActivity().run {
            ViewModelProviders.of(this, viewModelFactory)[SharedDataViewModel::class.java]
        }.apply {
            calendarSelectedEvent.observe(viewLifecycleOwner, Observer {
                getViewModel().updateDate(it.data)
            })
        }
        binding.viewModel = getViewModel()
        getViewModel().initialize(viewLifecycleOwner)
        binding.lifecycleOwner = viewLifecycleOwner
        this.binding = binding*/
    }

}