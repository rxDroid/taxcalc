package com.rxdroid.feature_add_expense

data class ExpenseViewData(
    val expense: String = "",
    val category: String = "",
    val bookingDate: String = "",
    val submitEnabled: Boolean = false
)
