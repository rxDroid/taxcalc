package com.rxdroid.feature_add_expense.di

import androidx.lifecycle.ViewModel
import com.rxdroid.feature_base.di.ViewModelKey
import com.rxdroid.feature_add_expense.AddExpenseViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AddExpenseViewModule {

    @Binds
    @IntoMap
    @ViewModelKey(AddExpenseViewModel::class)
    abstract fun viewModel(viewModel: AddExpenseViewModel): ViewModel
}
