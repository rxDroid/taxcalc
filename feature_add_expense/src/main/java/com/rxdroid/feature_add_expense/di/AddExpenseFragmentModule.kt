package com.rxdroid.feature_add_expense.di

import com.rxdroid.feature_add_expense.AddExpenseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AddExpenseFragmentModule {

    @ContributesAndroidInjector(modules = [AddExpenseViewModule::class])
    abstract fun contributeFragment(): AddExpenseFragment
}