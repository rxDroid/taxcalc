package com.rxdroid.feature_base

import androidx.navigation.NavDirections

sealed class NavigationCommand {

    class To(val directions: NavDirections) : NavigationCommand()

    object BackHome : NavigationCommand()
}
