package com.rxdroid.feature_base.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.rxdroid.feature_base.NavigationCommand
import com.rxdroid.feature_base.SingleLiveEvent

open class BaseViewModel<D>(initial: D? = null) : ViewModel() {

    private val data: MutableLiveData<D> = MutableLiveData()

    fun viewData(): LiveData<D> = data

    internal val navigationCommands = SingleLiveEvent<NavigationCommand>()

    init {
        data.value = initial
    }

    protected fun update(initial: () -> D = { error("null initial state") }, block: D.() -> D) {
        val input = data.value ?: initial()
        val update = block(input)
        if (input != update) data.value = update
    }

    fun navigate(directions: NavDirections) {
        navigationCommands.postValue(NavigationCommand.To(directions))
    }

}
