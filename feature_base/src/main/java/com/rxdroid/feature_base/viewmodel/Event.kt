package com.rxdroid.feature_base.viewmodel

data class Event<T>(
    val data: T
)
