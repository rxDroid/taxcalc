package com.rxdroid.feature_base.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*
import javax.inject.Inject

class SharedDataViewModel @Inject constructor() : ViewModel() {

    val calendarSelectedEvent = MutableLiveData<Event<Calendar>>()

}
