package com.rxdroid.feature_base

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

private val currencyFormat = NumberFormat.getCurrencyInstance(Locale.GERMANY)
private val dateFormat = SimpleDateFormat("dd-MM-yyyy")

fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

inline fun <T> LiveData<T>.withAutoObserver(
    owner: LifecycleOwner,
    crossinline observer: (T?) -> Unit
) {
    observe(owner, Observer { observer(it) })
}

fun formatDate(date: Date): String = dateFormat.format(date)

fun formatPercentage(percentage: Double?): String {
    return if (percentage != null) {
        "%.2f".format(percentage) + "%"
    } else {
        "0 %"
    }
}

fun formatCurrency(value: Double?): String {
    return if (value != null) {
        currencyFormat.format(value)
    } else {
        "0.00 €"
    }
}

fun String.isDouble(): Boolean = Regex("-?\\d+(.\\d+)?").matches(this)

fun View.visibleOrGone(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun EditText.onTextChanged(onTextChanged: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
        override fun afterTextChanged(editable: Editable?) = Unit
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            onTextChanged(p0.toString())
        }
    })
}

fun SwitchCompat.onCheckChanged(onCheckChanged: (Boolean) -> Unit) {
    setOnCheckedChangeListener { _, isChecked -> onCheckChanged(isChecked) }
}


