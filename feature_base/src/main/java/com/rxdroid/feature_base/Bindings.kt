package com.rxdroid.feature_base

import android.text.Editable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText

object Bindings {

    @BindingAdapter("visibleOrGone")
    @JvmStatic
    fun View.setVisibleOrGone(show: Boolean) {
        visibility = if (show) VISIBLE else GONE
    }

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun setImageUrl(view: ImageView, url: String) {
        if (url.isNotEmpty()) {
            Glide.with(view.context)
                .asDrawable()
                .load(url)
                .into(view)
        }
    }

    @BindingAdapter("srcVector")
    @JvmStatic
    fun setSrcVector(view: ImageView, @DrawableRes drawable: Int) {
        view.setImageResource(drawable)
    }

    @BindingAdapter("onTextUpdate")
    @JvmStatic
    fun onTextUpdate(view: TextInputEditText, value : Editable?) {
        view.text = value
    }

}
