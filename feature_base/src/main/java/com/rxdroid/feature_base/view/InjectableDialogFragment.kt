package com.rxdroid.feature_base.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.rxdroid.feature_base.NavigationCommand
import com.rxdroid.feature_base.viewmodel.BaseViewModel
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerDialogFragment
import javax.inject.Inject

abstract class InjectableDialogFragment<Binding : ViewDataBinding, VM: BaseViewModel<*>> : DaggerDialogFragment(),
    ViewProvider<Binding>,
    BaseViewModelProvider<VM>{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewBinding: Binding
    private lateinit var viewModel: VM

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModelClazz())
        initBinding(viewBinding)
        viewModel.navigationCommands.observe(viewLifecycleOwner, Observer { navCommand ->
            when (navCommand) {
                is NavigationCommand.To -> findNavController().navigate(navCommand.directions)
            }
        })
    }


    override fun getViewModel(): VM = viewModel

    abstract fun getViewModelClazz(): Class<VM>
}
