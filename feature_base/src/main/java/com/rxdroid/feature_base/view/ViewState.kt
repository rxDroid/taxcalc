package com.rxdroid.feature_base.view

enum class ViewState {

    ERROR, INIT, LOADING, NO_RESULTS, DATA_LOADED

}
