package com.rxdroid.feature_base.view

import com.rxdroid.feature_base.R
import com.rxdroid.lib_repository.base.DataOperation
import com.rxdroid.lib_repository.base.DataOperation.*
import com.rxdroid.lib_repository.base.RepoError
import com.rxdroid.lib_repository.base.RepoError.*


sealed class StorageViewError : ViewError {

    companion object {

        fun create(repoError: RepoError): ViewError = when (repoError) {
            is Storage -> mapStorageError(repoError.operation)
            General -> TODO()
            NoConnection -> TODO()
            NoResults -> TODO()
            Server -> TODO()
            Authentication -> TODO()
            TimeOut -> TODO()
        }

        private fun mapStorageError(operation: DataOperation): ViewError = when (operation) {
            READ -> ReadError
            WRITE -> WriteError
            DELETE -> DeleteError
            UPDATE -> UpdateError
        }

    }

    object ReadError : StorageViewError() {

        override fun getDrawableResId(): Int = R.drawable.ic_suspicious

        override fun getTitleResId(): Int = R.string.error_title

        override fun getMessageResId(): Int = R.string.error_storage_read_text
    }

    object DeleteError : StorageViewError() {

        override fun getDrawableResId(): Int = R.drawable.ic_suspicious

        override fun getTitleResId(): Int = R.string.error_title

        override fun getMessageResId(): Int = R.string.error_storage_delete_text
    }

    object WriteError : StorageViewError() {

        override fun getDrawableResId(): Int = R.drawable.ic_suspicious

        override fun getTitleResId(): Int = R.string.error_title

        override fun getMessageResId(): Int = R.string.error_storage_insert_text
    }

    object UpdateError : StorageViewError() {

        override fun getDrawableResId(): Int = R.drawable.ic_suspicious

        override fun getTitleResId(): Int = R.string.error_title

        override fun getMessageResId(): Int = R.string.error_storage_update_text
    }

}
