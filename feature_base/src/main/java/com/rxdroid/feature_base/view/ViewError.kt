package com.rxdroid.feature_base.view

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

interface ViewError{

    @DrawableRes
    fun getDrawableResId(): Int

    @StringRes
    fun getTitleResId(): Int

    @StringRes
    fun getMessageResId(): Int

}
