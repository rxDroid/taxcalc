package com.rxdroid.feature_base.view

import androidx.databinding.ViewDataBinding

internal interface ViewProvider<in Binding : ViewDataBinding> {

    fun getLayoutId(): Int

    fun initBinding(binding: Binding)

}
