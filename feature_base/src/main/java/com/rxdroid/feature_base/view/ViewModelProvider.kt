package com.rxdroid.feature_base.view

import com.rxdroid.feature_base.viewmodel.BaseViewModel

internal interface BaseViewModelProvider<VM: BaseViewModel<*>>{

    fun getViewModel() : VM

}
