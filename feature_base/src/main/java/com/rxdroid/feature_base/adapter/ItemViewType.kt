package com.rxdroid.feature_base.adapter

interface ItemViewType {

    fun getItemViewType(): Int
}