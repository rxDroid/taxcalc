package com.rxdroid.feature_base.di

import com.rxdroid.feature_base.view.DatePickerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CommonFragmentModule {

    @ContributesAndroidInjector(modules = [SharedViewModule::class])
    abstract fun contributeFragment(): DatePickerFragment
}