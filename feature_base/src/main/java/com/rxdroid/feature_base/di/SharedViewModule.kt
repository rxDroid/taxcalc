package com.rxdroid.feature_base.di

import androidx.lifecycle.ViewModel
import com.rxdroid.feature_base.viewmodel.SharedDataViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SharedViewModule {

    @Binds
    @IntoMap
    @ViewModelKey(SharedDataViewModel::class)
    abstract fun viewModel(viewModel: SharedDataViewModel): ViewModel
}
