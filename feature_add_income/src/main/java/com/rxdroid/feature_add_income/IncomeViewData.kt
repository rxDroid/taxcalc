package com.rxdroid.feature_add_income

import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.view.ViewError
import com.rxdroid.lib_repository.data.Income

data class IncomeViewData(
    val viewState: ViewState = ViewState.INIT,
    val submitEnabled: Boolean = false,
    val isRevenueTaxIncluded: Boolean = true,
    val userInput: Double = 0.0,
    val data: Income = Income(),
    val viewError: ViewError? = null
)
