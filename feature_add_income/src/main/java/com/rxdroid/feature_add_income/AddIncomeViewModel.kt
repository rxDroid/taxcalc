package com.rxdroid.feature_add_income

import androidx.lifecycle.viewModelScope
import androidx.navigation.ActionOnlyNavDirections
import com.rxdroid.feature_base.isDouble
import com.rxdroid.feature_base.view.StorageViewError
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.viewmodel.BaseViewModel
import com.rxdroid.lib_repository.AddIncomeRepositoryImpl
import com.rxdroid.lib_repository.Tax
import com.rxdroid.lib_repository.base.DataOperation
import com.rxdroid.lib_repository.base.RepoError
import com.rxdroid.lib_repository.base.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class AddIncomeViewModel @Inject constructor(
    private val addIncomeRepository: AddIncomeRepositoryImpl
) : BaseViewModel<IncomeViewData>(IncomeViewData()) {

    @ExperimentalCoroutinesApi
    fun onSubmitClicked() {
        val income = viewData().value?.data
        income?.let {
            viewModelScope.launch {
                addIncomeRepository.storeIncome(it)
                    .onStart {
                        update {
                            copy(
                                viewState = ViewState.LOADING,
                                submitEnabled = false
                            )
                        }
                    }
                    .catch { emit(Result.Error(RepoError.create(DataOperation.WRITE, it))) }
                    .collect { result ->
                        when (result) {
                            is Result.Success -> {
                                handleSuccess(result)
                            }
                            is Result.Error -> {
                                handleError(result.repoError)
                            }
                        }
                    }
            }
        }
    }

    fun onDateClicked() {
        navigate(ActionOnlyNavDirections(R.id.nav_action_add_income_fragment_to_datePickerFragment))
    }

    fun onCheckedChanged(isChecked: Boolean) {
        update {
            copy(isRevenueTaxIncluded = isChecked)
        }
        refresh()
    }

    fun onIncomeUpdate(value: String?) {
        if (value?.isNotBlank() == true && value.isDouble()) {
            val input = value.toDouble()
            update {
                copy(userInput = input)
            }
            refresh()
        } else {
            update {
                copy(submitEnabled = false)
            }
        }
    }

    fun onClientUpdate(input: String?) {
        update {
            copy(data = data.copy(client = input))
        }
    }

    fun updateDate(calendar: Calendar) {
        update {
            copy(data = data.copy(bookingDate = calendar))
        }
    }

    private fun refresh() {
        val input = viewData().value?.userInput ?: 0.0
        val revenueTax = calcRevenueTax(input)
        val revenueIncome = calcRevenueIncome(input, revenueTax)
        val grossIncome = calcGrossIncome(input, revenueTax)
        update {
            copy(
                submitEnabled = true,
                data = data.copy(
                    revenueIncome = revenueIncome,
                    revenueTax = revenueTax,
                    grossIncome = grossIncome
                )
            )
        }
    }

    private fun calcRevenueTax(input: Double): Double =
        if (viewData().value?.isRevenueTaxIncluded == true) {
            Tax.RevenueIncluded.calculateTax(input)
        } else {
            Tax.Revenue.calculateTax(input)
        }

    private fun calcRevenueIncome(
        input: Double,
        revenueTax: Double
    ): Double = if (viewData().value?.isRevenueTaxIncluded == true) {
        input
    } else {
        input + revenueTax
    }

    private fun calcGrossIncome(
        input: Double,
        revenueTax: Double
    ): Double = if (viewData().value?.isRevenueTaxIncluded == true) {
        input - revenueTax
    } else {
        input
    }

    private fun handleError(repoError: RepoError) {
        update {
            copy(
                viewState = ViewState.ERROR,
                viewError = StorageViewError.create(repoError),
                submitEnabled = true
            )
        }
    }

    private fun handleSuccess(result: Result.Success<Boolean>) {
        if (result.data) {
            navigate(ActionOnlyNavDirections(R.id.nav_action_back_to_main))
        }
    }


}
