package com.rxdroid.feature_add_income.di

import com.rxdroid.feature_add_income.AddIncomeFragment
import com.rxdroid.feature_add_income.di.AddIncomeViewModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AddIncomeFragmentModule {

    @ContributesAndroidInjector(modules = [AddIncomeViewModule::class])
    abstract fun contributeFragment(): AddIncomeFragment
}