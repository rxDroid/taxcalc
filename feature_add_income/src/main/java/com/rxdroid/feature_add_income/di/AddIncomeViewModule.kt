package com.rxdroid.feature_add_income.di

import androidx.lifecycle.ViewModel
import com.rxdroid.feature_base.di.ViewModelKey
import com.rxdroid.feature_add_income.AddIncomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AddIncomeViewModule {

    @Binds
    @IntoMap
    @ViewModelKey(AddIncomeViewModel::class)
    abstract fun viewModel(viewModel: AddIncomeViewModel): ViewModel
}