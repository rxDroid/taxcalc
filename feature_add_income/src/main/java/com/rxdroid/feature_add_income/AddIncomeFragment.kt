package com.rxdroid.feature_add_income

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.rxdroid.feature_base.*
import com.rxdroid.feature_base.view.InjectableNavFragment
import com.rxdroid.feature_base.view.ViewState
import com.rxdroid.feature_base.viewmodel.SharedDataViewModel
import com.rxdroid.feature_add_income.databinding.FragmentAddIncomeBinding
import com.rxdroid.feature_base.view.ViewError


class AddIncomeFragment : InjectableNavFragment<FragmentAddIncomeBinding, AddIncomeViewModel>() {

    private lateinit var binding: FragmentAddIncomeBinding

    override fun getViewModelClazz(): Class<AddIncomeViewModel> = AddIncomeViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_add_income

    override fun initBinding(binding: FragmentAddIncomeBinding) {
        requireActivity().run {
            ViewModelProviders.of(this, viewModelFactory)[SharedDataViewModel::class.java]
        }.apply {
            calendarSelectedEvent.observe(viewLifecycleOwner, Observer {
                getViewModel().updateDate(it.data)
            })
        }
        binding.viewModel = getViewModel()
        binding.lifecycleOwner = viewLifecycleOwner
        binding.toolbar.apply {
            setNavigationOnClickListener { activity?.onBackPressed() }
            title = "Add Income"
        }
        this.binding = binding
        getViewModel().apply {
            viewData().observe(viewLifecycleOwner, ::updateUi)
        }
        binding.includedAddIncome.incomeInput.onTextChanged { getViewModel().onIncomeUpdate(it) }
        binding.includedAddIncome.clientEditText.onTextChanged { getViewModel().onClientUpdate(it) }
        binding.includedAddIncome.saleTaxSwitch.onCheckChanged { getViewModel().onCheckedChanged(it) }
    }

    private fun updateUi(viewData: IncomeViewData) {
        toggleLoadingView(viewData.viewState)
        toggleErrorView(viewData.viewState, viewData.viewError)
        binding.includedAddIncome.saleText.text = formatCurrency(viewData.data.revenueTax)
        binding.includedAddIncome.dateButton.text = formatDate(viewData.data.bookingDate.time)
    }

    private fun toggleLoadingView(viewState: ViewState) {
        binding.includedLoading.view.visibleOrGone(viewState == ViewState.LOADING)
    }

    private fun toggleErrorView(viewState: ViewState, viewError: ViewError?) {
        binding.includedError.view.visibleOrGone(viewState == ViewState.ERROR)
        viewError?.let {
            binding.includedError.title.text = getText(it.getTitleResId())
            binding.includedError.message.text = getText(it.getMessageResId())
            binding.includedError.icon.setImageResource(it.getDrawableResId())
        }
    }

}
