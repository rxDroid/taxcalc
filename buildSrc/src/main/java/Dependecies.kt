@Suppress("unused")
object Versions {
    const val min_sdk = 21
    const val build_tools = "29.0.2"
    const val target_sdk = 29
    const val compile_sdk = 29
    const val version_code = 1
    const val version_name = "1.0"

    const val kotlin = "1.3.70"
    const val safe_args = "2.1.0"

    const val glide = "4.9.0"
    const val room = "2.2.1"

    const val android_gradle_plugin = "3.6.0"
    const val androidx = "1.1.0"
    const val androidx_fragment = "1.2.0-rc01"
    const val androidx_material = "1.1.0-alpha07"
    const val constraint_layout = "1.1.3"
    const val androidx_navigation = "2.1.0"
    const val livedata_ktx = "2.1.0"
    const val dagger2 = "2.25.2"

    //test version
    const val junit = "4.12"
    const val test_runner = "1.2.0"
    const val espresso = "3.2.0"
}

@Suppress("unused")
object Deps {
    const val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val android_gradle_plugin =
        "com.android.tools.build:gradle:${Versions.android_gradle_plugin}"
    const val safe_args_plugin =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.safe_args}"

    const val androidx_appcompat = "androidx.appcompat:appcompat:${Versions.androidx}"
    const val androidx_core = "androidx.core:core-ktx:${Versions.androidx}"
    const val androidx_livedata =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.livedata_ktx}"
    const val androidx_lifecycle_ext =
        "androidx.lifecycle:lifecycle-extensions:${Versions.livedata_ktx}"

    const val androidx_fragment = "androidx.fragment:fragment-ktx:${Versions.androidx_fragment}"
    const val androidx_material =
        "com.google.android.material:material:${Versions.androidx_material}"
    const val constraint_layout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraint_layout}"

    const val navigation_fragment =
        "androidx.navigation:navigation-fragment-ktx:${Versions.androidx_navigation}"
    const val navigation_ui =
        "androidx.navigation:navigation-ui-ktx:${Versions.androidx_navigation}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glide_compiler = "com.github.bumptech.glide:compiler:${Versions.glide}"

    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val room_ktx = "androidx.room:room-ktx:${Versions.room}"
    const val room_compiler = "androidx.room:room-compiler:${Versions.room}"

    const val dagger = "com.google.dagger:dagger-android:${Versions.dagger2}"
    const val dagger_android_support =
        "com.google.dagger:dagger-android-support:${Versions.dagger2}"
    const val dagger_android_processor = "com.google.dagger:dagger-android-processor:${Versions.dagger2}"
    const val dagger_compiler = "com.google.dagger:dagger-compiler:${Versions.dagger2}"

}

object TestLibraries {

    const val junit = "junit:junit:${Versions.junit}"
    const val test_runner = "androidx.test:runner:${Versions.test_runner}"
    const val espresso_core = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}
