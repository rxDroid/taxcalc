package com.rxdroid.taxcalc

import com.rxdroid.feature_base.view.InjectableActivity

class MainActivity : InjectableActivity() {

    override fun getLayoutId(): Int = R.layout.activity_main

}
