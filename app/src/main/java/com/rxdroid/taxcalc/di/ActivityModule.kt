package com.rxdroid.taxcalc.di

import com.rxdroid.feature_base.di.CommonFragmentModule
import com.rxdroid.feature_add_expense.di.AddExpenseFragmentModule
import com.rxdroid.feature_add_income.di.AddIncomeFragmentModule
import com.rxdroid.feature_details.di.DetailsFragmentModule
import com.rxdroid.feature_main.di.MainFragmentModule
import com.rxdroid.taxcalc.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(
        modules = [
            CommonFragmentModule::class,
            AddIncomeFragmentModule::class,
            AddExpenseFragmentModule::class,
            DetailsFragmentModule::class,
            MainFragmentModule::class
        ]
    )
    abstract fun contributeNavHostActivity(): MainActivity
}
