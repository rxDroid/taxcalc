package com.rxdroid.taxcalc.di

import android.content.Context
import com.rxdroid.api.di.ApiModule
import com.rxdroid.feature_add_expense.di.AddExpenseViewModule
import com.rxdroid.feature_add_income.di.AddIncomeViewModule
import com.rxdroid.feature_base.di.CommonUiModule
import com.rxdroid.feature_base.di.SharedViewModule
import com.rxdroid.feature_details.di.DetailsViewModule
import com.rxdroid.feature_main.di.MainViewModule
import com.rxdroid.lib_data.di.DataModule
import com.rxdroid.lib_repository.di.RepositoryModule
import com.rxdroid.taxcalc.TaxApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        CommonUiModule::class,
        SharedViewModule::class,
        RepositoryModule::class,
        AddIncomeViewModule::class,
        AddExpenseViewModule::class,
        DetailsViewModule::class,
        MainViewModule::class,
        DataModule::class,
        ApiModule::class
    ]
)
interface AppComponent : AndroidInjector<TaxApplication> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }

}
