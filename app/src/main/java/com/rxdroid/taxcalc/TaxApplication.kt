package com.rxdroid.taxcalc

import com.rxdroid.taxcalc.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class TaxApplication : DaggerApplication(){

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = DaggerAppComponent.factory()
        .create(applicationContext)
}