package com.rxdroid.lib_data

import androidx.room.TypeConverter
import java.util.*

class Converters {

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun fromTimestamp(value: Long?): Calendar?{
        return value?.let { Calendar.getInstance()}
    }
}
