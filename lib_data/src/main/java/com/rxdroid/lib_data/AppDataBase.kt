package com.rxdroid.lib_data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rxdroid.lib_data.daos.ExpenseEntityDao
import com.rxdroid.lib_data.daos.IncomeEntityDao
import com.rxdroid.lib_data.entities.ExpenseEntity
import com.rxdroid.lib_data.entities.IncomeEntity

@Database(
    entities = [
        IncomeEntity::class,
        ExpenseEntity::class
    ], version = 1
)
@TypeConverters(Converters::class)
abstract class AppDataBase : RoomDatabase() {

    internal abstract fun taxEntityDao(): IncomeEntityDao
    internal abstract fun expenseEntityDao(): ExpenseEntityDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: AppDataBase? = null

        fun getDatabase(context: Context): AppDataBase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDataBase::class.java,
                    "app_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}
