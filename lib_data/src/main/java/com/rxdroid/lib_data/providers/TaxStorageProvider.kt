package com.rxdroid.lib_data.providers

import com.rxdroid.lib_data.dtos.IncomeDto
import kotlinx.coroutines.flow.Flow

interface TaxStorageProvider {

    suspend fun insert(incomeDto: IncomeDto): Flow<Unit>

    suspend fun update(incomeDto: IncomeDto): Flow<Unit>

    suspend fun delete(incomeDto: IncomeDto): Flow<Unit>

    suspend fun getAll(): Flow<List<IncomeDto>>

}
