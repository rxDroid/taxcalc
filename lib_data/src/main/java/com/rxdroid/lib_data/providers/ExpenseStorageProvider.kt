package com.rxdroid.lib_data.providers

import com.rxdroid.lib_data.dtos.ExpenseDto
import kotlinx.coroutines.flow.Flow

interface ExpenseStorageProvider {

    suspend fun insert(dto: ExpenseDto): Flow<Unit>

    suspend fun update(dto: ExpenseDto): Flow<Unit>

    suspend fun delete(dto: ExpenseDto): Flow<Unit>

    suspend fun getAll(): Flow<List<ExpenseDto>>

}
