package com.rxdroid.lib_data.providers

import android.content.Context
import com.rxdroid.lib_data.AppDataBase
import com.rxdroid.lib_data.dtos.ExpenseDto
import com.rxdroid.lib_data.entities.ExpenseEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ExpenseStorageProviderImpl @Inject constructor(context: Context) : ExpenseStorageProvider {

    private var dataBase: AppDataBase = AppDataBase.getDatabase(context)

    override suspend fun insert(dto: ExpenseDto): Flow<Unit> = flow {
        dataBase.expenseEntityDao().insert(ExpenseEntity.fromDto(dto))
        emit(Unit)
    }

    override suspend fun update(dto: ExpenseDto): Flow<Unit> = flow {
        dataBase.expenseEntityDao().update(ExpenseEntity.fromDto(dto))
        emit(Unit)
    }

    override suspend fun delete(dto: ExpenseDto): Flow<Unit> = flow {
        dataBase.expenseEntityDao().delete(ExpenseEntity.fromDto(dto))
        emit(Unit)
    }

    override suspend fun getAll(): Flow<List<ExpenseDto>> = flow {
        val dtoList = ArrayList<ExpenseDto>()
        val entities = dataBase.expenseEntityDao().getAll()
        entities.forEach {
            dtoList.add(it.toDto())
        }
        emit(dtoList)
    }

}
