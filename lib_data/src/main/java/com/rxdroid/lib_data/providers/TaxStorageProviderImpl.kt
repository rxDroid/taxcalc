package com.rxdroid.lib_data.providers

import android.content.Context
import com.rxdroid.lib_data.AppDataBase
import com.rxdroid.lib_data.dtos.IncomeDto
import com.rxdroid.lib_data.entities.IncomeEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class TaxStorageProviderImpl @Inject constructor(context: Context) : TaxStorageProvider {

    private var dataBase: AppDataBase = AppDataBase.getDatabase(context)

    override suspend fun insert(incomeDto: IncomeDto): Flow<Unit> = flow {
        dataBase.taxEntityDao().insert(IncomeEntity.fromDto(incomeDto))
        emit(Unit)
    }

    override suspend fun update(incomeDto: IncomeDto): Flow<Unit> = flow {
        dataBase.taxEntityDao().update(IncomeEntity.fromDto(incomeDto))
        emit(Unit)
    }

    override suspend fun delete(incomeDto: IncomeDto): Flow<Unit> = flow {
        dataBase.taxEntityDao().delete(IncomeEntity.fromDto(incomeDto))
        emit(Unit)
    }

    override suspend fun getAll(): Flow<List<IncomeDto>> = flow {
        val dtoList = ArrayList<IncomeDto>()
        val entities = dataBase.taxEntityDao().getAll()
        entities.forEach {
            dtoList.add(it.toDto())
        }
        emit(dtoList)
    }

}
