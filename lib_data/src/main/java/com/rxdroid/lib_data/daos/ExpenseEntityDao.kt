package com.rxdroid.lib_data.daos

import androidx.room.*
import com.rxdroid.lib_data.entities.ExpenseEntity

@Dao
internal interface ExpenseEntityDao {

    @Query("SELECT * FROM ExpenseEntity")
    suspend fun getAll(): List<ExpenseEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: ExpenseEntity)

    @Update
    suspend fun update(entity: ExpenseEntity)

    @Delete
    suspend fun delete(entity: ExpenseEntity)

}
