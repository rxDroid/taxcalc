package com.rxdroid.lib_data.daos

import androidx.room.*
import com.rxdroid.lib_data.entities.IncomeEntity

@Dao
internal interface IncomeEntityDao {

    @Query("SELECT * FROM IncomeEntity")
    suspend fun getAll(): List<IncomeEntity>

    @Insert
    suspend fun insert(entity: IncomeEntity)

    @Update
    suspend fun update(entity: IncomeEntity)

    @Delete
    suspend fun delete(entity: IncomeEntity)

}
