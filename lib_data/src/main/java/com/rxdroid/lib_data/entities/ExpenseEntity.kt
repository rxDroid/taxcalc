package com.rxdroid.lib_data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rxdroid.lib_data.dtos.ExpenseDto

@Entity(tableName = "ExpenseEntity")
internal data class ExpenseEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long?,
    @ColumnInfo(name = "expense") val expense: Double?,
    @ColumnInfo(name = "category") val category: String?,
    @ColumnInfo(name = "booking_year") val bookingYear: Int,
    @ColumnInfo(name = "booking_month") val bookingMonth: Int,
    @ColumnInfo(name = "booking_day") val bookingDay: Int
) {
    companion object {
        fun fromDto(expenseDto: ExpenseDto): ExpenseEntity = ExpenseEntity(
            id = null,
            expense = expenseDto.expense,
            category = expenseDto.category,
            bookingYear = expenseDto.bookingYear,
            bookingMonth = expenseDto.bookingMonth,
            bookingDay = expenseDto.bookingDay
        )
    }

    fun toDto(): ExpenseDto = ExpenseDto(
        expense = expense ?: 0.0,
        category = category ?: "",
        bookingYear = bookingYear,
        bookingMonth = bookingMonth,
        bookingDay = bookingDay
    )
}
