package com.rxdroid.lib_data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rxdroid.lib_data.dtos.IncomeDto

@Entity(tableName = "IncomeEntity")
internal data class IncomeEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long?,
    @ColumnInfo(name = "revenue_income") val revenueIncome: Double,
    @ColumnInfo(name = "revenue_tax") val revenueTax: Double,
    @ColumnInfo(name = "gross_income") val grossIncome: Double,
    @ColumnInfo(name = "booking_year") val bookingYear: Int,
    @ColumnInfo(name = "booking_month") val bookingMonth: Int,
    @ColumnInfo(name = "booking_day") val bookingDay: Int,
    @ColumnInfo(name = "client") val client: String?
) {
    companion object {
        fun fromDto(dto: IncomeDto): IncomeEntity = IncomeEntity(
            id = null,
            revenueIncome = dto.revenueIncome,
            revenueTax = dto.revenueTax,
            grossIncome = dto.grossIncome,
            bookingYear = dto.bookingYear,
            bookingMonth = dto.bookingMonth,
            bookingDay = dto.bookingDay,
            client = dto.client
        )
    }

    fun toDto(): IncomeDto = IncomeDto(
        id = id,
        revenueIncome = revenueIncome,
        revenueTax = revenueTax,
        grossIncome = grossIncome,
        bookingYear = bookingYear,
        bookingMonth = bookingMonth,
        bookingDay = bookingDay,
        client = client
    )

}
