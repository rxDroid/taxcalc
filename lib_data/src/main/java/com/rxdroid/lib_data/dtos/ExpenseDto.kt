package com.rxdroid.lib_data.dtos

data class ExpenseDto(
    val expense: Double,
    val category: String,
    val bookingYear: Int,
    val bookingMonth: Int,
    val bookingDay: Int
)
