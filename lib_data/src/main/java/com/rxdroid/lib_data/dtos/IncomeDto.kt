package com.rxdroid.lib_data.dtos

data class IncomeDto(
    val id: Long? = null,
    val revenueIncome: Double = 0.0,
    val revenueTax: Double = 0.0,
    val grossIncome: Double = 0.0,
    val bookingYear: Int,
    val bookingMonth: Int,
    val bookingDay: Int,
    val client: String? = "Client"
)
