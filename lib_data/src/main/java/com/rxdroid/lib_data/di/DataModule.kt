package com.rxdroid.lib_data.di

import android.content.Context
import com.rxdroid.lib_data.providers.ExpenseStorageProvider
import com.rxdroid.lib_data.providers.ExpenseStorageProviderImpl
import com.rxdroid.lib_data.providers.TaxStorageProvider
import com.rxdroid.lib_data.providers.TaxStorageProviderImpl
import dagger.Module
import dagger.Provides

@Module
object DataModule {

    @JvmStatic
    @Provides
    fun provideTaxStorageProvider(context: Context): TaxStorageProvider =
        TaxStorageProviderImpl(context)


    @JvmStatic
    @Provides
    fun provideExpenseStorageProvider(context: Context): ExpenseStorageProvider =
        ExpenseStorageProviderImpl(context)
}
