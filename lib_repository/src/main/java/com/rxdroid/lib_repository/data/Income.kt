package com.rxdroid.lib_repository.data

import com.rxdroid.lib_data.dtos.IncomeDto
import java.util.*

data class Income(
    val id: Long? = null,
    val revenueIncome: Double = 0.0,
    val revenueTax: Double = 0.0,
    val grossIncome: Double = 0.0,
    val bookingDate: Calendar = Calendar.getInstance(),
    val client: String? = "Client"
) {

    companion object {

        fun fromDto(dto: IncomeDto): Income {
            val bookingCalendar = Calendar.getInstance()
            bookingCalendar.set(
                dto.bookingYear, dto.bookingMonth, dto.bookingDay
            )
            return Income(
                id = dto.id,
                revenueIncome = dto.revenueIncome,
                revenueTax = dto.revenueTax,
                grossIncome = dto.grossIncome,
                bookingDate = bookingCalendar,
                client = dto.client
            )
        }
    }
}

fun Income.toDto(): IncomeDto =
    IncomeDto(
        revenueIncome = revenueIncome,
        revenueTax = revenueTax,
        grossIncome = grossIncome,
        bookingYear = bookingDate.get(Calendar.YEAR),
        bookingMonth = bookingDate.get(Calendar.MONTH),
        bookingDay = bookingDate.get(Calendar.DAY_OF_MONTH),
        client = client
    )
