package com.rxdroid.lib_repository.data

import java.util.*

data class Expense(
    val grossIncome: Double = 0.0,
    val saleTax: Double = 0.0,
    val isSaleTaxIncluded: Boolean = true,
    val netIncome: Double = 0.0,
    val category: String = "",
    val bookingDate: Calendar = Calendar.getInstance()
)