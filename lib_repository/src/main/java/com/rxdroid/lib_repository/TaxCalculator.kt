package com.rxdroid.lib_repository

internal interface TaxCalculator {

    fun calculateTax(income: Double): Double

}
