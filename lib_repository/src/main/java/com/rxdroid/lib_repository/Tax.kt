package com.rxdroid.lib_repository

sealed class Tax : TaxCalculator {

    object Revenue : Tax() {

        override fun calculateTax(income: Double): Double = income * 0.19
    }

    object RevenueIncluded : Tax() {

        override fun calculateTax(income: Double): Double = income - (income / 1.19)
    }

    object Income : Tax() {

        override fun calculateTax(income: Double): Double {
            val taxLevel = getTaxLevelForIncome(income)
            return taxLevel.calculateTax(income)
        }

        private fun getTaxLevelForIncome(income: Double): TaxLevel {
            return if (income < 9169) {
                TaxLevel.One
            } else if (income > 9169 && income < 14255) {
                TaxLevel.Two
            } else if (income > 14255 && income < 54961) {
                TaxLevel.Three
            } else if (income > 54961 && income < 265326) {
                TaxLevel.Four
            } else {
                TaxLevel.Five
            }
        }

    }

    object Solidarity : Tax() {

        override fun calculateTax(income: Double): Double = income * 0.055

    }

}
