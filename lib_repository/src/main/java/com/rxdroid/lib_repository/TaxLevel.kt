package com.rxdroid.lib_repository

internal sealed class TaxLevel : TaxCalculator {

    object One : TaxLevel() {
        override fun calculateTax(income: Double): Double =
            0.00
    }

    object Two : TaxLevel() {
        override fun calculateTax(income: Double): Double =
            (980.14 * ((income - 9168) / 10000) + 1400) * ((income - 9168) / 10000)
    }

    object Three : TaxLevel() {
        override fun calculateTax(income: Double): Double =
            (216.16 * ((income - 14254) / 10000) + 2397) * ((income - 14254) / 10000) + 965.58
    }

    object Four : TaxLevel() {
        override fun calculateTax(income: Double): Double =
            0.42 * income - 8780.9
    }

    object Five : TaxLevel() {
        override fun calculateTax(income: Double): Double =
            0.45 * income - 16740.68
    }

}
