package com.rxdroid.lib_repository

import com.rxdroid.lib_data.dtos.ExpenseDto
import com.rxdroid.lib_data.providers.ExpenseStorageProvider
import com.rxdroid.lib_repository.base.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.util.*
import javax.inject.Inject

@Suppress("EXPERIMENTAL_API_USAGE")
class AddExpenseRepository @Inject constructor(
    private val storageProvider: ExpenseStorageProvider
) {

    suspend fun storeExpense(
        expense: Double,
        bookingCalendar: Calendar
    ): Flow<Result<Boolean>> {
        val expenseDto = ExpenseDto(
            expense = expense,
            category = "TODO",
            bookingYear = bookingCalendar.get(Calendar.YEAR),
            bookingMonth = bookingCalendar.get(Calendar.MONTH),
            bookingDay = bookingCalendar.get(Calendar.DAY_OF_MONTH)
        )
        return storageProvider.insert(expenseDto)
            .catch { throwable ->
                //forward exception to VM
                 throw throwable
            }
            .map {
                Result.Success(true)
            }
    }
}
