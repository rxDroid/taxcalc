package com.rxdroid.lib_repository.base

enum class DataOperation {

    READ, WRITE, DELETE, UPDATE
}