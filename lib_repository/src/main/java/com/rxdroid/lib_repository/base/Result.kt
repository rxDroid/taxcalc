package com.rxdroid.lib_repository.base


sealed class Result<out T> {

    data class Success<out T>( val data: T) : Result<T>()

    data class Error(val repoError: RepoError) : Result<Nothing>()

}
