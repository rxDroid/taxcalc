package com.rxdroid.lib_repository.base

import java.net.ConnectException
import java.net.HttpURLConnection.*
import java.net.UnknownHostException

sealed class RepoError {

    companion object {

        fun create(throwable: Throwable?): RepoError = when (throwable) {
            is ConnectException, is UnknownHostException -> NoConnection
            else -> General
        }

        fun create(errorCode: Int): RepoError = when (errorCode) {
            HTTP_NOT_FOUND -> NoResults
            HTTP_UNAUTHORIZED -> Authentication
            HTTP_CLIENT_TIMEOUT -> TimeOut
            HTTP_INTERNAL_ERROR -> Server
            else -> General
        }

        fun create(operation: DataOperation, throwable: Throwable? = null) = Storage(operation, throwable)
    }

    object General : RepoError()

    object NoConnection : RepoError()

    object NoResults : RepoError()

    object Server : RepoError()

    object Authentication : RepoError()

    object TimeOut : RepoError()

    data class Storage(val operation: DataOperation, val throwable: Throwable?) : RepoError()
}
