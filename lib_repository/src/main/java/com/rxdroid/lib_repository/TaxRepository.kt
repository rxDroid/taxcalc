package com.rxdroid.lib_repository

import com.rxdroid.lib_repository.base.Result
import com.rxdroid.lib_repository.data.Income
import kotlinx.coroutines.flow.Flow

interface TaxRepository{

    suspend fun loadAll() : Flow<Result<List<Income>>>

    suspend fun loadAllByYear(bookingYear : Int) : Flow<Result<List<Income>>>
}
