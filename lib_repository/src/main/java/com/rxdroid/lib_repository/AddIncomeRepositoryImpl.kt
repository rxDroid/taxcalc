package com.rxdroid.lib_repository

import com.rxdroid.lib_data.providers.TaxStorageProvider
import com.rxdroid.lib_repository.base.Result
import com.rxdroid.lib_repository.data.Income
import com.rxdroid.lib_repository.data.toDto
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class AddIncomeRepositoryImpl @Inject constructor(
    private val taxStorageProvider: TaxStorageProvider
) : AddIncomeRepository {

    override suspend fun storeIncome(income: Income): Flow<Result<Boolean>> =
        taxStorageProvider.insert(income.toDto())
            .map {
                Result.Success(true)
            }

}
