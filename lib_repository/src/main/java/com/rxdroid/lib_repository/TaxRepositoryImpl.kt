package com.rxdroid.lib_repository

import com.rxdroid.lib_data.dtos.IncomeDto
import com.rxdroid.lib_data.providers.TaxStorageProvider
import com.rxdroid.lib_repository.base.Result
import com.rxdroid.lib_repository.data.Income
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

@Suppress("EXPERIMENTAL_API_USAGE")
class TaxRepositoryImpl @Inject constructor(private val storageProvider: TaxStorageProvider) : TaxRepository {

    override suspend fun loadAll(): Flow<Result<List<Income>>> =
        storageProvider.getAll()
            .transform { emit(mapResult(it)) }

    override suspend fun loadAllByYear(bookingYear: Int): Flow<Result<List<Income>>> =
        storageProvider.getAll()
            .map { dtoList ->
                dtoList.filter { it.bookingYear == bookingYear }
            }
            .transform { emit(mapResult(it)) }


    private fun mapResult(dtos: List<IncomeDto>): Result<List<Income>> =
        if (dtos.isEmpty()) {
            Result.Success(emptyList())
        } else {
            val incomeList = ArrayList<Income>()
            dtos.forEach {
                incomeList.add(Income.fromDto(it))
            }
            Result.Success(incomeList)
        }

}
