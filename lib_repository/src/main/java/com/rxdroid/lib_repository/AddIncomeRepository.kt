package com.rxdroid.lib_repository

import com.rxdroid.lib_repository.base.Result
import com.rxdroid.lib_repository.data.Income
import kotlinx.coroutines.flow.Flow

interface AddIncomeRepository {

    suspend fun storeIncome(income: Income): Flow<Result<Boolean>>

}
