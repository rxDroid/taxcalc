package com.rxdroid.lib_repository.di

import com.rxdroid.lib_data.providers.TaxStorageProvider
import com.rxdroid.lib_repository.AddIncomeRepository
import com.rxdroid.lib_repository.AddIncomeRepositoryImpl
import com.rxdroid.lib_repository.TaxRepository
import com.rxdroid.lib_repository.TaxRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
object RepositoryModule {

    @JvmStatic
    @Provides
    fun provideAddIncomeViewRepository(taxStorageProvider: TaxStorageProvider): AddIncomeRepository =
        AddIncomeRepositoryImpl(taxStorageProvider)

    @JvmStatic
    @Provides
    fun provideTaxRepository(taxStorageProvider: TaxStorageProvider): TaxRepository =
        TaxRepositoryImpl(taxStorageProvider)

}
